import 'package:flutter/material.dart';
import 'package:river_tz/core/app.dart';

void main() {
  runApp(
    const App(),
  );
}
