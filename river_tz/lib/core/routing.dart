import 'package:flutter/material.dart';
import 'package:river_tz/core/constants/routes.dart';
import 'package:river_tz/features/news/domain/models/news.dart';
import 'package:river_tz/features/news/presentation/pages/news_detail_page.dart';
import 'package:river_tz/features/news/presentation/pages/news_page.dart';

class Routing {
  static Route<dynamic>? onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Routes.initial:
        return MaterialPageRoute(
          builder: (context) => const NewsPage(),
        );
      case Routes.newsDetails:
        return MaterialPageRoute(
          builder: (context) => NewsDetailPage(
            news: settings.arguments as News,
          ),
        );
    }
    return null;
  }
}
