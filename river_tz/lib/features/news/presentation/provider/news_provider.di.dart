import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:river_tz/features/news/domain/usecases/news_usecases.di.dart';
import 'package:river_tz/features/news/presentation/provider/news_notifier.dart';
import 'package:river_tz/features/news/presentation/provider/news_state.dart';

final newsNotifierProvider = StateNotifierProvider<NewsNotifier, NewsState>(
  (ref) => NewsNotifier(
    getFeaturedNewsUseCase: ref.read(getFeaturedNewsUseCaseProvider),
    getLatestNewsUseCase: ref.read(getLatestNewsUseCaseProvider),
  ),
);
