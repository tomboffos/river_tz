import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:river_tz/features/news/domain/models/news.dart';

part 'news_state.freezed.dart';

@freezed
class NewsState with _$NewsState {
  factory NewsState.initial() = _Initial;

  factory NewsState.loaded({
    @Default([]) List<News> featuredNews,
    @Default([]) List<News> latestNews,
  }) = _Loaded;

  factory NewsState.error({String? error}) = _Error;
}
