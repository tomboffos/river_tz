import 'package:river_tz/features/news/domain/models/news.dart';
import 'package:river_tz/features/news/domain/usecases/get_featured_news_usecase.dart';
import 'package:river_tz/features/news/domain/usecases/get_latest_news_usecase.dart';
import 'package:river_tz/features/news/presentation/provider/news_state.dart';
import 'package:riverpod/riverpod.dart';

class NewsNotifier extends StateNotifier<NewsState> {
  final GetFeaturedNewsUseCase getFeaturedNewsUseCase;
  final GetLatestNewsUseCase getLatestNewsUseCase;

  NewsNotifier({
    required this.getFeaturedNewsUseCase,
    required this.getLatestNewsUseCase,
  }) : super(NewsState.initial()) {
    onInit();
  }

  Future<void> onInit() async {
    try {
      final featured = await getFeaturedNewsUseCase.execute();

      final latest = await getLatestNewsUseCase.execute();

      state = NewsState.loaded(
        featuredNews: featured,
        latestNews: latest,
      );
    } on Exception {
      state = NewsState.error();
    }
  }

  void readAll() async {
    state.whenOrNull(
      loaded: (featured, latest) {
        state = NewsState.loaded(
          featuredNews: featured.map((e) => e.copyWith(isRead: true)).toList(),
          latestNews: latest.map((e) => e.copyWith(isRead: true)).toList(),
        );
      },
    );
  }

  void readOneNews(News news) async {
    state.whenOrNull(
      loaded: (featured, latest) {
        state = NewsState.loaded(
          featuredNews: featured
              .map((e) => e == news ? e.copyWith(isRead: true) : e)
              .toList(),
          latestNews: latest
              .map((e) => e == news ? e.copyWith(isRead: true) : e)
              .toList(),
        );
      },
    );
  }
}
