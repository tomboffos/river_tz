import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:river_tz/features/news/presentation/provider/news_provider.di.dart';
import 'package:river_tz/features/news/presentation/widgets/news_item.dart';

class NewsPage extends ConsumerWidget {
  const NewsPage({super.key});

  @override
  Widget build(
    BuildContext context,
    WidgetRef ref,
  ) {
    final state = ref.watch(newsNotifierProvider);
    final newsNotifier = ref.read(newsNotifierProvider.notifier);
    return Scaffold(
      appBar: AppBar(
        title: const Text('News'),
        actions: [
          ElevatedButton(
            onPressed: newsNotifier.readAll,
            child: const Text('Read all'),
          ),
        ],
      ),
      body: state.when(
        initial: () => const Center(
          child: CircularProgressIndicator(),
        ),
        loaded: (featured, latest) => SingleChildScrollView(
          child: Column(
            children: [
              const Text(
                'Featured',
                style: TextStyle(
                  fontSize: 30,
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height / 5,
                padding: const EdgeInsets.only(left: 20),
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) => NewsItem(
                    news: featured[index],
                    onTap: newsNotifier.readOneNews,
                  ),
                  itemCount: featured.length,
                ),
              ),
              const SizedBox(
                height: 50,
              ),
              const Text(
                'Latest',
                style: TextStyle(
                  fontSize: 30,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: latest.length,
                  itemBuilder: (context, index) => NewsItem(
                    news: latest[index],
                    onTap: newsNotifier.readOneNews,
                  ),
                ),
              ),
            ],
          ),
        ),
        error: (error) => const Center(
          child: Text('Error'),
        ),
      ),
    );
  }
}
