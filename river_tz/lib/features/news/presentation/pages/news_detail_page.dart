import 'package:flutter/material.dart';
import 'package:river_tz/features/news/domain/models/news.dart';

class NewsDetailPage extends StatelessWidget {
  final News news;

  const NewsDetailPage({
    super.key,
    required this.news,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(news.name),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.network(
                  news.image,
                  height: 100,
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(news.name),
                const SizedBox(
                  height: 20,
                ),
                Text(news.description),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
