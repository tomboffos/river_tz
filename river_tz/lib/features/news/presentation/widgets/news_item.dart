import 'package:flutter/material.dart';
import 'package:river_tz/core/constants/routes.dart';
import 'package:river_tz/features/news/domain/models/news.dart';

class NewsItem extends StatelessWidget {
  final News news;
  final Function(News) onTap;

  const NewsItem({
    super.key,
    required this.news,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTap.call(news);

        Navigator.of(context).pushNamed(
          Routes.newsDetails,
          arguments: news,
        );
      },
      child: Card(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
            children: [
              Image.network(
                news.image,
                height: 80,
              ),
              const SizedBox(
                height: 20,
              ),
              Text(news.name),
              if (!news.isRead) const Text('Not readed')
            ],
          ),
        ),
      ),
    );
  }
}
