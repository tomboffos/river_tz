import 'package:freezed_annotation/freezed_annotation.dart';

part 'news.g.dart';
part 'news.freezed.dart';

@freezed
class News with _$News {
  factory News({
    required String name,
    required String description,
    required String image,
    @Default(false) bool isRead,
  }) = _News;

  factory News.fromJson(Map<String, dynamic> json) => _$NewsFromJson(json);
}
