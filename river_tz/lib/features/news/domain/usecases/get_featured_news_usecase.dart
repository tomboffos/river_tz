import 'package:river_tz/features/news/domain/models/news.dart';
import 'package:river_tz/features/news/domain/repository/news_repository.dart';

final class GetFeaturedNewsUseCase {
  final NewsRepository newsRepository;

  GetFeaturedNewsUseCase({required this.newsRepository});

  Future<List<News>> execute() => newsRepository.getFeaturedNews();
}
