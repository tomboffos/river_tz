import 'package:river_tz/features/news/domain/repository/news_repository.di.dart';
import 'package:river_tz/features/news/domain/usecases/get_featured_news_usecase.dart';
import 'package:river_tz/features/news/domain/usecases/get_latest_news_usecase.dart';
import 'package:riverpod/riverpod.dart';

final getFeaturedNewsUseCaseProvider = Provider<GetFeaturedNewsUseCase>(
  (ref) => GetFeaturedNewsUseCase(
    newsRepository: ref.read(newsRepositryProvider),
  ),
);

final getLatestNewsUseCaseProvider = Provider<GetLatestNewsUseCase>(
  (ref) => GetLatestNewsUseCase(
    newsRepository: ref.read(newsRepositryProvider),
  ),
);
