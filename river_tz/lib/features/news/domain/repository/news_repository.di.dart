import 'package:river_tz/features/news/data/data_source/news_data_source.di.dart';
import 'package:river_tz/features/news/domain/repository/news_repository.dart';
import 'package:river_tz/features/news/domain/repository/news_repository_impl.dart';
import 'package:riverpod/riverpod.dart';

final newsRepositryProvider = Provider<NewsRepository>(
  (ref) => NewsRepositoryImpl(
    newsDataSource: ref.read(
      newsDataSourceProvider,
    ),
  ),
);
