import 'package:river_tz/features/news/domain/models/news.dart';

abstract class NewsRepository {
  Future<List<News>> getFeaturedNews();

  Future<List<News>> getLatestNews();
}
