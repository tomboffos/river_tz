import 'package:river_tz/features/news/data/data_source/news_data_source.dart';
import 'package:river_tz/features/news/domain/models/news.dart';
import 'package:river_tz/features/news/domain/repository/news_repository.dart';

class NewsRepositoryImpl implements NewsRepository {
  final NewsDataSource newsDataSource;

  NewsRepositoryImpl({required this.newsDataSource});

  @override
  Future<List<News>> getFeaturedNews() async {
    final data = await newsDataSource.getFeaturedNews();

    return data.map((e) => News.fromJson(e)).toList();
  }

  @override
  Future<List<News>> getLatestNews() async {
    final data = await newsDataSource.getLatestNews();

    return data.map((e) => News.fromJson(e)).toList();
  }
}
