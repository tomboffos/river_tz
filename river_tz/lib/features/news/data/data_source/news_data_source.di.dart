import 'package:river_tz/features/news/data/data_source/news_data_source.dart';
import 'package:river_tz/features/news/data/data_source/news_data_source_impl.dart';
import 'package:riverpod/riverpod.dart';

final newsDataSourceProvider =
    Provider<NewsDataSource>((ref) => NewsDataSourceImpl());
