import 'package:river_tz/features/news/data/data_source/news_data_source.dart';

class NewsDataSourceImpl implements NewsDataSource {
  final List mockData = [
    {
      "name": "News 1",
      "description": "News description 1",
      "image": "https://imgupscaler.com/images/samples/animal-after.webp",
    },
    {
      "name": "News 2",
      "description": "News description 2",
      "image": "https://imgupscaler.com/images/samples/animal-after.webp",
    },
    {
      "name": "News 3",
      "description": "News description 3",
      "image": "https://imgupscaler.com/images/samples/animal-after.webp",
    },
    {
      "name": "News 4",
      "description": "News description 4",
      "image": "https://imgupscaler.com/images/samples/animal-after.webp",
    },
    {
      "name": "News 5",
      "description": "News description 5",
      "image": "https://imgupscaler.com/images/samples/animal-after.webp",
    },
  ];

  final List featured = [
    {
      "name": "News featured 1",
      "description": "News featured description 1",
      "image": "https://imgupscaler.com/images/samples/animal-after.webp",
    },
    {
      "name": "News featured 2",
      "description": "News featured description 2",
      "image": "https://imgupscaler.com/images/samples/animal-after.webp",
    },
    {
      "name": "News featured 3",
      "description": "News featured description 3",
      "image": "https://imgupscaler.com/images/samples/animal-after.webp",
    },
    {
      "name": "News featured 4",
      "description": "News featured description 4",
      "image": "https://imgupscaler.com/images/samples/animal-after.webp",
    },
    {
      "name": "News featured 5",
      "description": "News featured description 5",
      "image": "https://imgupscaler.com/images/samples/animal-after.webp",
    },
  ];

  @override
  Future<List> getFeaturedNews() => Future.value(featured);

  @override
  Future<List> getLatestNews() => Future.value(mockData);
}
