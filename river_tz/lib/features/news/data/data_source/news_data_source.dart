abstract class NewsDataSource {
  Future<List> getFeaturedNews();
  Future<List> getLatestNews();
}
